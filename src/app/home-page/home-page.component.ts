import { BudgetItem } from './../shared/models/budget-item.model';
import { Component, OnInit } from '@angular/core';
import { UpdateEvent } from '../item-list/item-list.component';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  items: BudgetItem[] = new Array<BudgetItem>();
  totalBudget: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

  addItem(item: BudgetItem) {
    this.items.push(item);
    if(item.amount) {
      this.totalBudget += item.amount;
    }
  }

  deleteItem(item: BudgetItem) {
    let index = this.items.indexOf(item);
    this.items.splice(index, 1);

    if(item.amount) {
      this.totalBudget -= item.amount;
    }
  }

  updateItem(updateEvent: UpdateEvent) {
    // result is the update budget item
    // replace the item with the updated/submitted item from the form
    this.items[this.items.indexOf(updateEvent.old)] = updateEvent.new;

    // update the total budget
    if(updateEvent.old.amount && updateEvent.new.amount) {
      this.totalBudget -= updateEvent.old.amount;
      this.totalBudget += updateEvent.new.amount;
    }
  }

}
