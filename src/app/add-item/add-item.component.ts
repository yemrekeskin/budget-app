import { BudgetItem } from './../shared/models/budget-item.model';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss']
})
export class AddItemComponent implements OnInit {

  @Input() item: BudgetItem;
  @Output() submitted : EventEmitter<BudgetItem> = new EventEmitter<BudgetItem>();
  isNewItem: boolean;

  constructor() { }

  ngOnInit(): void {
     // if item has a value
     if (this.item) {
      // this means that an existing item object was passed into this component
      // therefore this is not a new item
      this.isNewItem = false;
    } else {
      this.isNewItem = true;
      this.item = new BudgetItem('', null);
    }
  }

  onSubmit(form: NgForm) {
    console.log(form);
    console.log(this.item);

    let newItem = new BudgetItem(form.value.description, form.value.amount);

    this.submitted.emit(newItem);
    // this.submitted.emit(form.value);
    // this.submitted.emit(this.item);

    form.reset();
  }

}
